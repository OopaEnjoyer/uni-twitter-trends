module TwitterTrends.Projection where

open import TwitterTrends.Float
open import TwitterTrends.LatLong
open import TwitterTrends.Plane.V2

record AlbersParams : Set where
    field
        reference : LatLong
        φ₁ φ₂     : Float -- standard parallels

    φ₀ λ₀ : Float
    φ₀ = LatLong.latitude reference
    λ₀ = LatLong.longitude reference

albers : AlbersParams → LatLong → V2
albers params (φλ 𝜑 𝜆) = v2 (ρ * sin θ) (ρ₀ - ρ * cos θ)
    where
    open AlbersParams params

    n θ C ρ ρ₀ : Float
    n = 0.5 * (sin φ₁ + sin φ₂)
    θ = n * (𝜆 - λ₀)
    C = cos² φ₁ + 2.0 * n * sin φ₁
    ρ = sqrt (C - 2.0 * n * sin 𝜑) ÷ n
    ρ₀ = sqrt (C - 2.0 * n * sin φ₀) ÷ n

albers⁻¹ : AlbersParams → V2 → LatLong
albers⁻¹ params (v2 x y) = φλ 𝜑 𝜆
    where
    open AlbersParams params

    C ρ ρ₀ n θ : Float
    n = 0.5 * (sin φ₁ + sin φ₂)
    C = cos² φ₁ + 2.0 * n * sin φ₁
    ρ₀ = sqrt (C - 2.0 * n * sin φ₀) ÷ n

    ρ = sqrt (x * x + (ρ₀ - y) ** 2.0)
    θ = atan (x ÷ (ρ₀ - y))

    𝜑 𝜆 : Float
    𝜑 = asin ((C - ρ * ρ * n * n) ÷ (2.0 * n))
    𝜆 = λ₀ + θ ÷ n
