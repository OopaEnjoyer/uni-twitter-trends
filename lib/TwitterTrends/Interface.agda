module TwitterTrends.Interface where

open import Data.Float using (Float)
open import Data.List using (List)
open import Data.Maybe using (Maybe)
open import Data.Product using (_×_)
open import Data.String using (String)
open import Foreign.Haskell.Pair using (Pair)
open import TwitterTrends.Color using (Color)
open import TwitterTrends.LatLong using (LatLong)
open import TwitterTrends.Plane.Line
open import TwitterTrends.Plane.V2

WordEval : Set
WordEval = String → Maybe Float

record Output : Set where 
    constructor output
    field
        states : List (Pair (List (List Line)) (Pair Color (Pair V2 String)))
        tweets : List (Pair V2 Color)

record Input : Set where
    constructor input
    field
        states : List (Pair (List (List LatLong)) String)
        tweets : List (Pair LatLong String)
        evalp  : WordEval

{-# FOREIGN GHC
    import MAlonzo.Code.TwitterTrends.Plane.V2
    import MAlonzo.Code.TwitterTrends.Plane.Line
    import MAlonzo.Code.TwitterTrends.LatLong
    import MAlonzo.Code.TwitterTrends.Color

    type AgdaWordEval = Data.Text.Text -> Maybe Double
    data AgdaOutput = AgdaOutput [([[AgdaLine]], (AgdaColor, (AgdaV2, Data.Text.Text)))] [(AgdaV2, AgdaColor)]
    data AgdaInput = AgdaInput [([[AgdaLatLong]], Data.Text.Text)] [(AgdaLatLong, Data.Text.Text)] AgdaWordEval
#-}
{-# COMPILE GHC Output = data AgdaOutput( AgdaOutput ) #-}
{-# COMPILE GHC Input = data AgdaInput( AgdaInput ) #-}