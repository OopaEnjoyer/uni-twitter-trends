module TwitterTrends.LatLong where

open import Data.Float using (Float)

record LatLong : Set where
    constructor φλ
    field
        latitude longitude : Float

{-# FOREIGN GHC
    data AgdaLatLong = AgdaLatLong Double Double
#-}

{-# COMPILE GHC LatLong = data AgdaLatLong( AgdaLatLong ) #-}