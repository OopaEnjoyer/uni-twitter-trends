module TwitterTrends.Test where

import TwitterTrends.Plane.AABB as BB
open import Data.Bool using (Bool; true; false; _∧_)
open import Data.List using (_∷_; [])
open import Data.Product using (_×_; _,_)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)
open import TwitterTrends.Float
open import TwitterTrends.LatLong using (LatLong; φλ)
open import TwitterTrends.Plane.V2 using (V2; v2)

-- Poly

module Poly where
    open import TwitterTrends.Plane.Poly
    
    ply1 : PolyFat
    ply1 = fatFromPts (v2 0.0 0.0 ∷ v2 1.0 0.0 ∷ v2 1.0 1.0 ∷ v2 0.0 1.0 ∷ [])

    _ : ply1 ∋ v2 0.5 0.5 ≡ true
    _ = refl

    _ : ply1 ∋ v2 1.1 0.3 ≡ false
    _ = refl

    ply2 : PolyFat
    ply2 = fatFromPts (v2 0.0 0.0 ∷ v2 1.0 0.0 ∷ v2 -1.0 -1.0 ∷ [])

    _ : ply2 ∋ v2 0.5 -0.1 ≡ true
    _ = refl

    _ : ply2 ∋ v2 1.1 0.3 ≡ false
    _ = refl 

-- Albers Projection

module Albers where
    open import TwitterTrends.Projection
    
    _ll≈_upto_ : LatLong → LatLong → Float → Bool
    (φλ φ₁ λ₁) ll≈ (φλ φ₂ λ₂) upto ε = (φ₁ ≈ φ₂ upto ε) ∧ (λ₁ ≈ λ₂ upto ε)

    albersTest : AlbersParams → LatLong → Float → Set
    albersTest ap ll ε = (albers⁻¹ ap (albers ap ll)) ll≈ ll upto ε ≡ true  

    testParams₁ : AlbersParams
    testParams₁ = record
        { reference = φλ (deg⇒rad 66.0) (deg⇒rad 105.0)
        ; φ₁        = deg⇒rad 52.0
        ; φ₂        = deg⇒rad 64.0
        }

    _ : albersTest testParams₁ (φλ (deg⇒rad 60.5) (deg⇒rad 108.0)) 1e-323
    _ = refl
