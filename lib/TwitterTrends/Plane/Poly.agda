module TwitterTrends.Plane.Poly where

open import Data.Bool using (Bool; true; false; _∧_; if_then_else_)
open import Data.List as L using (List; []; _∷_)
open import Data.Nat using (ℕ; suc)
open import Data.Nat.DivMod using (_%_)
open import Data.Product using (_×_; _,_; proj₁; proj₂)
open import Data.String using (String)
open import Data.Sum using (inj₁; inj₂)
open import Function using (_∘_; _$_)
open import TwitterTrends.Float
open import TwitterTrends.Plane.AABB as BB using (AABB)
open import TwitterTrends.Plane.Line as Line using (Line; LineK; line; linek)
open import TwitterTrends.Plane.V2

Poly : Set
Poly = List Line

PolyFat : Set
PolyFat = List (Line × LineK) × AABB

fromPts : Pts → Poly
fromPts []             = []
fromPts (_ ∷ [])       = []
fromPts (p₀ ∷ p₁ ∷ ps) = aux p₀ (p₀ ∷ p₁ ∷ ps)
    where
    aux : V2 → Pts → Poly
    aux p₀ []               = []
    aux p₀ (pₙ ∷ [])        = (line pₙ p₀) ∷ []
    aux p₀ (pᵢ ∷ pᵢ₊₁ ∷ ps) = (line pᵢ pᵢ₊₁) ∷ aux p₀ (pᵢ₊₁ ∷ ps)

fatFromPts : Pts → PolyFat
fatFromPts pts = L.map (λ l → l , Line.coeff l) (fromPts pts) , BB.fromPts pts  

_∋_ : PolyFat → V2 → Bool
(ply , bb) ∋ (v2 x y) =
    if bb BB.∋ v2 x y
    then (ℕ⇒Bool ∘ (_% 2) ∘ L.sum) $ L.map (Bool⇒ℕ ∘ ∩_) $ ply
    else false
    where
    Bool⇒ℕ : Bool → ℕ
    Bool⇒ℕ false = 0
    Bool⇒ℕ true  = 1

    ℕ⇒Bool : ℕ → Bool     -- n % 2 ~~ 1 is "inside", 0 is "outside"
    ℕ⇒Bool 0       = false
    ℕ⇒Bool (suc _) = true

    ∩_ : Line × LineK → Bool
    ∩ (line (v2 lx₁ ly₁) (v2 lx₂ ly₂) , (linek a b c)) =
        (x ≤ᵇ x₀) ∧ (lx⊓ ≤ᵇ x₀) ∧ (x₀ ≤ᵇ lx⊔)
        where
        x₀ lx⊔ lx⊓ : Float
        x₀ = (- b * y - c) ÷ a

        lx⊔ = lx₁ ⊔ lx₂
        lx⊓ = lx₁ ⊓ lx₂