module TwitterTrends.Plane.State where

import TwitterTrends.Plane.AABB as BB
open import Data.Bool using (Bool)
open import Data.List as L using (List)
open import Data.Product using (_×_; _,_; proj₁; proj₂)
open import Function using (_∘_)
open import TwitterTrends.Plane.Line as Line using ()
open import TwitterTrends.Plane.Poly as Ply using (Poly; PolyFat)
open import TwitterTrends.Plane.V2 using (V2; v2; centroid; Pts)

State : Set
State = List PolyFat × V2

from[Pts] : List Pts → State
from[Pts] L.[]            = L.[] , (v2 0.0 0.0)
from[Pts] (ply₀ L.∷ plys) = L.map Ply.fatFromPts (ply₀ L.∷ plys) , centroid ply₀

to[Poly] : State → List Poly
to[Poly] = L.map ((L.map proj₁) ∘ proj₁) ∘ proj₁

_∋_ : State → V2 → Bool
sf ∋ p = L.any (Ply._∋ p) (proj₁ sf)
