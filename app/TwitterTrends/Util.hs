module TwitterTrends.Util where

import Data.Ord (clamp)

maprange :: (Double, Double) -> (Double, Double) -> Double -> Double
maprange (min1, max1) (min2, max2) v =
    clamp (min2, max2) $ (v - min1) / (max1 - min1) * (max2 - min2) + min2
