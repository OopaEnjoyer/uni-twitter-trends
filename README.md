Twitter Trends (CS61A Fall 2014)

# Prerequiesites

Programs:

* GHC, tested with 9.2.1
* Agda, tested with 2.6.2.1
* Cabal, >= 2.4

Agda libraries:

* standard-library-2.0 (https://github.com/agda/agda-stdlib)

# Build

```
./build_agda
cabal build twitter-trends
```

# Run

Put **states.json**, **sentiments.csv** and tweet files to **data/**; **DejaVuSans.ttf** to **fonts/**.

```
cabal run
```

# Note

* "family_tweets2014.txt" contains invalid line(?) (search by "Vogel"), parsing it results in an error
