module TwitterTrends.Plane.Line where

open import Data.Product using (_×_; _,_)
open import TwitterTrends.Float
open import TwitterTrends.Plane.V2

data Line : Set where
    line : V2 → V2 → Line

{-# FOREIGN GHC
    import MAlonzo.Code.TwitterTrends.Plane.V2
    data AgdaLine = AgdaLine AgdaV2 AgdaV2
#-}

{-# COMPILE GHC Line = data AgdaLine( AgdaLine ) #-}

data LineK : Set where
    linek : Float → Float → Float → LineK

coeff : Line → LineK
coeff (line (v2 x₁ y₁) (v2 x₂ y₂)) = linek a b c
    where
        a b c : Float
        a = y₁ - y₂
        b = x₂ - x₁
        c = x₁ * y₂ - x₂ * y₁

f : LineK → V2 → Float
f (linek a b c) (v2 x y) = a * x + b * y + c
