module TwitterTrends.Trends where

open import Data.Bool using (Bool; true; false; _∨_; if_then_else_)
open import Data.Char as Ch using (Char)
open import Data.List as L using (List; []; _∷_)
open import Data.Maybe using (Maybe; just; nothing)
open import Data.Nat as ℕ using (ℕ; suc)
open import Data.Product as Σ using (_×_; _,_; proj₁; proj₂)
open import Data.String as S using (String)
open import Foreign.Haskell.Coerce using (coerce)
open import Function using (_∘_; _$_)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)
open import TwitterTrends.Color as C using (Color)
open import TwitterTrends.Float
open import TwitterTrends.Interface
open import TwitterTrends.LatLong
open import TwitterTrends.Plane.Line as Line using (Line; LineK; line; linek)
open import TwitterTrends.Plane.State as State using (State)
open import TwitterTrends.Plane.V2 using (V2)
open import TwitterTrends.Projection using (AlbersParams; albers)

albersParams : AlbersParams
albersParams = record
    { reference = φλ (deg⇒rad 50.707953) (deg⇒rad -101.026027)
    ; φ₁        = deg⇒rad 23.0
    ; φ₂        = deg⇒rad 43.0
    }

filter : ∀{a}{A : Set a} → (A → Bool) → List A → List A
filter f [] = []
filter f (x ∷ xs) with f x
... | true  = x ∷ filter f xs
... | false = filter f xs

eval : WordEval → String → Float
eval w = Σ.uncurry (λ s n → if n ℕ.≡ᵇ 0 then 0.0 else s ÷ fromℕ n) ∘
    L.foldl (λ (t , n) → λ{ (just s) → t + s , suc n; nothing → t , n } ) (0.0 , 0) ∘
    L.map (w ∘ S.fromList ∘ L.map Ch.toLower) ∘ L.wordsBy (Ch._≟ ' ') ∘
    filter (λ c → (Ch.isAlpha c) ∨ (c Ch.≈ᵇ ' ')) ∘ S.toList

_ : eval (\w -> just (fromℕ (S.length w))) "word lngth avg" ≡ 4.0
_ = refl

kcolor : Maybe Float → Color
kcolor nothing  = Color.rgb 0.5 0.5 0.5
kcolor (just k) = findcl colormap
    where
    colormap : List (Float × Color)
    colormap = (-0.8 , Color.rgb 0.0 0.0 1.0) L.∷
               (-0.6 , Color.rgb 0.0 0.0 0.8) L.∷
               (-0.4 , Color.rgb 0.0 0.0 0.6) L.∷
               (-0.2 , Color.rgb 0.0 0.1 0.4) L.∷
               ( 0.0 , Color.rgb 0.0 0.2 0.2) L.∷
               ( 0.2 , Color.rgb 0.1 0.3 0.2) L.∷
               ( 0.4 , Color.rgb 0.3 0.4 0.1) L.∷
               ( 0.6 , Color.rgb 0.5 0.5 0.0) L.∷
               ( 0.8 , Color.rgb 0.7 0.7 0.0) L.∷
               ( 1.0 , Color.rgb 1.0 1.0 0.0) L.∷ L.[]

    findcl : List (Float × Color) -> Color
    findcl L.[]               = Color.rgb 0.0 0.0 0.0
    findcl ((_ , c) L.∷ L.[]) = c
    findcl ((v , c) L.∷ xs)   = if k <ᵇ v then c else findcl xs 

main : WordEval → List (List (List LatLong)) → List (LatLong × String) → List (State × Color) × List (V2 × Color)
main ep ss = Σ.map₁ (L.map (Σ.map₂ (λ (v , n) → kcolor $ if n ℕ.≡ᵇ 0 then nothing else just (v ÷ fromℕ n) ))) ∘ L.foldl fld (oss₀ , [])
    where
    ll⇒rad : LatLong → LatLong
    ll⇒rad (φλ 𝜑 𝜆) = φλ (deg⇒rad 𝜑) (deg⇒rad 𝜆) 

    stfs : List State
    stfs = L.map State.from[Pts] (L.map (L.map (L.map (albers albersParams ∘ ll⇒rad))) ss)

    oss₀ : List (State × Float × ℕ)
    oss₀ = L.map (λ stf → stf , 0.0 , 0) stfs
    
    fld : List (State × Float × ℕ) × List (V2 × Color) → (LatLong × String) → List (State × Float × ℕ) × List (V2 × Color)
    fld (oss , ots) t = aux oss , (tₓ , c) ∷ ots
        where
        tₓ : V2
        tₓ = albers albersParams (ll⇒rad (proj₁ t))
        
        tₛ : Float
        tₛ = eval ep (proj₂ t)

        c : Color
        c = kcolor $ just tₛ

        aux : List (State × Float × ℕ) → List (State × Float × ℕ)
        aux [] = []
        aux (s ∷ ss) with (proj₁ s) State.∋ tₓ
        ... | true  = (proj₁ s , ((proj₁ $ proj₂ s) + tₛ) , suc (proj₂ $ proj₂ s)) ∷ ss
        ... | false = s ∷ aux ss

logic : Input → Output
logic inp = let sts , stns = L.unzip $ L.map coerce states 
                oss , ots  = main evalp sts (coerce tweets)
            in record
            { states = L.map (λ (((sf , centr) , color) , stn) → coerce $ L.map (L.map proj₁ ∘ proj₁) sf , color , (centr , stn)) $ L.zip oss stns
            ; tweets = L.map coerce ots
            }
    where
    open Input inp

{-# COMPILE GHC logic as agdaTrends #-}