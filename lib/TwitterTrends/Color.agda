module TwitterTrends.Color where

open import Data.Float as Fl using (Float)
open import Data.Nat using (ℕ; suc; _≡ᵇ_)
open import Data.Bool using (if_then_else_)

data Color : Set where
    rgb : Float → Float → Float → Color

{-# FOREIGN GHC
    data AgdaColor = AgdaColor Double Double Double
#-}

{-# COMPILE GHC Color = data AgdaColor( AgdaColor ) #-}

getR : Color → Float
getR (rgb r g b) = r

getG : Color → Float
getG (rgb r g b) = g

getB : Color → Float
getB (rgb r g b) = b

black : Color
black = rgb 0.0 0.0 0.0

white : Color
white = rgb 1.0 1.0 1.0

_+_ : Color → Color → Color
rgb r₁ g₁ b₁ + rgb r₂ g₂ b₂  = rgb (r₁ Fl.+ r₂) (g₁ Fl.+ g₂) (b₁ Fl.+ b₂)

record AvgAcc : Set where
    field
        abs : Color
        num : ℕ

    value : Color
    value = if num ≡ᵇ 0 then rgb 0.0 0.0 0.0
                        else rgb (getR abs Fl.* n⁻¹) (getG abs Fl.* n⁻¹) (getB abs Fl.* n⁻¹)
        where
            n⁻¹ : Float
            n⁻¹ = 1.0 Fl.÷ Fl.fromℕ num

    app : Color → AvgAcc
    app δ = record { abs = abs + δ; num = suc num }
    