module TwitterTrends.Plane.V2 where

open import Data.List using (List; _∷_; [])
open import Data.Product as Σ using (_×_; _,_)
open import Function using (_∘_; _$_)
open import TwitterTrends.Float

data V2 : Set where
    v2 : Float → Float → V2

{-# FOREIGN GHC
    data AgdaV2 = AgdaV2 Double Double
#-}

{-# COMPILE GHC V2 = data AgdaV2( AgdaV2 ) #-}

Pts : Set
Pts = List V2

zero : V2
zero = v2 0.0 0.0

vop₂ : (Float → Float → Float) → V2 → V2 → V2
vop₂ _∙_ (v2 x₁ y₁) (v2 x₂ y₂) = v2 (x₁ ∙ x₂) (y₁ ∙ y₂)

infixl 6 _v+_
_v+_ : V2 → V2 → V2
_v+_ = vop₂ _+_

infixl 7.5 _v*s_
_v*s_ : V2 → Float → V2
(v2 x y) v*s s = v2 (x * s) (y * s)

infixl 7.5 _v/s_
_v/s_ : V2 → Float → V2
v v/s s = v v*s (1.0 ÷ s)

_v-_ : V2 → V2 → V2
_v-_ = vop₂ _-_

_v⊔_ : V2 → V2 → V2
_v⊔_ = vop₂ _⊔_

_v⊓_ : V2 → V2 → V2
_v⊓_ = vop₂ _⊓_

_v×_ : V2 → V2 → Float
(v2 u₁ u₂) v× (v2 v₁ v₂) = u₁ * v₂ - u₂ * v₁

centroid : Pts → V2
centroid []               = v2 0.0 0.0
centroid (a ∷ [])         = a
centroid (a ∷ b ∷ [])     = (a v+ b) v*s 0.5
centroid (a ∷ b ∷ c ∷ ps) = Σ.uncurry _v/s_ ∘ Σ.map₂ (_* 3.0) $ aux a (a ∷ b ∷ c ∷ ps)
    where
        auxₛ : V2 → V2 → V2 × Float
        auxₛ aᵢ aᵢ₊₁ = ((aᵢ v+ aᵢ₊₁) v*s area) , area
            where
            area : Float
            area = aᵢ v× aᵢ₊₁

        aux₊ : V2 × Float → V2 × Float → V2 × Float
        aux₊ (v₁ , s₁) (v₂ , s₂) = (v₁ v+ v₂ , s₁ + s₂)

        aux : V2 → Pts → V2 × Float
        aux p₀ []               = p₀ , 1.0  -- never 
        aux p₀ (pᵢ ∷ [])        = auxₛ pᵢ p₀
        aux p₀ (pᵢ ∷ pᵢ₊₁ ∷ ps) = aux₊ (auxₛ pᵢ pᵢ₊₁) (aux p₀ (pᵢ₊₁ ∷ ps))