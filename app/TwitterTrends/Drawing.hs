module TwitterTrends.Drawing where

import Codec.Picture (Image, generateImage, PixelRGBA8(..))
import Graphics.Rasterific
import Graphics.Rasterific.Texture
import MAlonzo.Code.TwitterTrends.Color (AgdaColor(AgdaColor))
import MAlonzo.Code.TwitterTrends.Plane.V2 (AgdaV2(AgdaV2))
import Graphics.Text.TrueType as Font
import Numeric (showFFloat)
import TwitterTrends.Config (outWidth, outHeight)
import TwitterTrends.Util (maprange)
import Data.Ord (clamp)
import Data.Word (Word8)
import qualified Data.Text
import Data.Text (Text)
import MAlonzo.Code.TwitterTrends.Plane.V2 (AgdaV2(AgdaV2))
import MAlonzo.Code.TwitterTrends.Plane.Line (AgdaLine(AgdaLine))
import MAlonzo.Code.TwitterTrends.LatLong (AgdaLatLong(AgdaLatLong))
import MAlonzo.Code.TwitterTrends.Color (AgdaColor(AgdaColor))
import MAlonzo.Code.TwitterTrends.Interface (AgdaOutput(AgdaOutput))

-- Const colors

clBlack :: PixelRGBA8
clBlack = PixelRGBA8 0 0 0 255

clWhite :: PixelRGBA8
clWhite = PixelRGBA8 255 255 255 255

-- Conversion

av2ToPoint :: AgdaV2 -> Point
av2ToPoint (AgdaV2 x y) = V2 (realToFrac $ outTransformX x) (realToFrac $ outTransformY (- y))

alineToLine :: AgdaLine -> Line
alineToLine (AgdaLine p1 p2) = Line (av2ToPoint p1) (av2ToPoint p2)

acolorToPixel :: AgdaColor -> PixelRGBA8
acolorToPixel (AgdaColor r g b) = PixelRGBA8 (denormColor r) (denormColor g) (denormColor b) 255
    where
    denormColor :: Double -> Word8
    denormColor d = clamp (0, 255) $ round (d * 255.0)

-- Utility

prettyShowCoeff :: Double -> String
prettyShowCoeff x = if x < 0.0 then s else ' ':s
    where
    s = showFFloat (Just 2) x ""

contrcolor :: AgdaColor -> AgdaColor
contrcolor (AgdaColor r g b) = AgdaColor outBrightness outBrightness outBrightness
    where
    inpBrightness = r + g + b
    outBrightness = if inpBrightness > 0.8 then 0.0 else 1.0

textOffset :: Font.BoundingBox -> Point -> Point
textOffset (Font.BoundingBox xm ym xM yM _) (V2 x y) = V2 (x - w * 0.5) (y - h * 0.5)
    where
    w = realToFrac $ xM - xm
    h = realToFrac $ yM - ym

outTransformX = maprange (-1, 1) (0, fromIntegral outWidth)
outTransformY = maprange (-1, 1) (0, fromIntegral outHeight)

-- Main

--                         Title
drawOutput :: Font.Font -> String -> AgdaOutput -> Image PixelRGBA8
drawOutput font title (AgdaOutput ss ts) = renderDrawing outWidth outHeight clWhite $ do
    let (drawBg, drawNames) = unzip $ map drawPoly ss
    sequence_ [drawLegend, sequence_ drawBg, sequence_ drawNames, drawTweets ts]
    withTexture (uniformTexture clBlack) $
       printTextAt font (PointSize 132) (V2 80 170) title
    where

    legend =    [ AgdaColor 0.0 0.0 1.0
                , AgdaColor 0.0 0.0 0.8
                , AgdaColor 0.0 0.0 0.6
                , AgdaColor 0.0 0.1 0.4
                , AgdaColor 0.0 0.2 0.2
                , AgdaColor 0.1 0.3 0.2
                , AgdaColor 0.3 0.4 0.1
                , AgdaColor 0.5 0.5 0.0
                , AgdaColor 0.7 0.7 0.0
                , AgdaColor 1.0 1.0 0.0
                ]

    drawLegend :: Drawing PixelRGBA8 ()
    drawLegend = flip mapM_ (zip3 legend [lh, 2 * lh..] [-0.8, -0.6..]) $ \(cl , iy, lv) -> do
        let ly = outHeight - iy
        withTexture (uniformTexture (acolorToPixel cl)) $
            fill $ rectangle (V2 0 ly) lw lh
        withTexture (uniformTexture clBlack) $
            printTextAt font (PointSize 104) (V2 (lw + 38) (ly - 23 + lh)) (prettyShowCoeff lv)

        where
            lw = 400
            lh = 150

    drawPoly :: ([[AgdaLine]], (AgdaColor, (AgdaV2, Text))) -> (Drawing PixelRGBA8 (), Drawing PixelRGBA8 ())
    drawPoly (plys, (color, (centroid, stnm))) = ((do
            withTexture (uniformTexture (acolorToPixel color)) $
                mapM_ (\ply -> fill $ map alineToLine ply) plys
            withTexture (uniformTexture clBlack) $
                mapM_ (\ply -> stroke 2.0 JoinRound (CapRound, CapRound) $ map alineToLine ply) plys
        ), (do
            let stnms = Data.Text.unpack stnm
                -- 96 - default rasterific DPI
                tbb = Font.stringBoundingBox font 96 (Font.pixelSizeInPointAtDpi 32.0 96) stnms

            withTexture (uniformTexture (acolorToPixel $ contrcolor color)) $ -- 
                printTextAt font (PointSize 32) (textOffset tbb $ av2ToPoint centroid) stnms
        ))

    drawTweets :: [(AgdaV2, AgdaColor)] -> Drawing PixelRGBA8 ()
    drawTweets ts = do
        flip mapM_ ts $ (\(pos, clr) -> do 
            withTexture (uniformTexture (acolorToPixel clr)) $
                fill $ circle (av2ToPoint pos) 3
            )

        flip mapM_ ts $ (\(pos, clr) -> do
            withTexture (uniformTexture $ acolorToPixel $ contrcolor clr) $
                stroke 1.0 JoinRound (CapRound, CapRound) $ circle (av2ToPoint pos) 3
            )
