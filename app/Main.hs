{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import Codec.Picture.Png (encodePng)
import Codec.Picture (Image, generateImage, PixelRGBA8(..)) 
import System.Directory (doesFileExist)
import MAlonzo.Code.TwitterTrends.Interface
import MAlonzo.Code.TwitterTrends.Trends (agdaTrends) 
import Graphics.Text.TrueType as Font
import qualified Data.ByteString as BS
import TwitterTrends.Config
import TwitterTrends.Parsing
import TwitterTrends.Drawing

main :: IO ()
main = do
    fontE <- Font.loadFontFile "./fonts/DejaVuSans.ttf"
    case fontE of
        Left  err  -> putStrLn ("Error loading font: " ++ err)
        Right font -> do
            putStrLn "Tweets file name without extension: "
            tweetsFname   <- getLine
            let tweetsFpath = "./data/" ++ tweetsFname ++ ".txt"
            tweetsFexists <- doesFileExist tweetsFpath
            if not tweetsFexists
                then putStrLn "Tweets file not found"
                else do
                    tweets   <- readTweets   tweetsFpath
                    wordEval <- loadWordEval "./data/sentiments.csv"
                    mstates  <- readStates   "./data/states.json"

                    flip (either $ \e -> putStrLn $ "States parsing failed: " ++ e) mstates
                        $ \states -> do
                            let agdaOutp = agdaTrends (AgdaInput states tweets wordEval)
                            BS.writeFile ("./img/" ++ tweetsFname ++ ".png")
                                $ BS.toStrict
                                $ encodePng
                                $ drawOutput font tweetsFname agdaOutp