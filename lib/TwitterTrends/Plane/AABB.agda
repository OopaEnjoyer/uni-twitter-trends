module TwitterTrends.Plane.AABB where

open import Data.Bool using (Bool; true; false; _∧_)
open import Data.List as L using (List)
open import TwitterTrends.Float
open import TwitterTrends.Plane.V2

record AABB : Set where
    constructor aabb
    field
        min max : V2

fromPts : Pts → AABB
fromPts = L.foldl (λ (aabb m M) p → aabb (p v⊓ m) (p v⊔ M)) (aabb zero zero)

_∋_ : AABB → V2 → Bool
aabb (v2 x₁ y₁) (v2 x₂ y₂) ∋ v2 x y = (x₁ <ᵇ x) ∧ (x <ᵇ x₂) ∧ (y₁ <ᵇ y) ∧ (y <ᵇ y₂)