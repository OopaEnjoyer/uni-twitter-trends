module TwitterTrends.Config where

outWidth :: Num a => a 
outWidth = 8192

outHeight :: Num a => a
outHeight = 8192