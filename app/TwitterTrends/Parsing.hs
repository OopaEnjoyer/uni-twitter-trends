{-# LANGUAGE ScopedTypeVariables #-}

module TwitterTrends.Parsing where

import Control.Monad (join)
import Data.List     (lines, foldl')
import Data.Text     (Text)
import Data.Vector   (Vector)
import MAlonzo.Code.TwitterTrends.Color      (AgdaColor(AgdaColor))
import MAlonzo.Code.TwitterTrends.Interface
import MAlonzo.Code.TwitterTrends.LatLong    (AgdaLatLong(AgdaLatLong))
import MAlonzo.Code.TwitterTrends.Plane.Line (AgdaLine(AgdaLine))
import MAlonzo.Code.TwitterTrends.Plane.V2   (AgdaV2(AgdaV2))
import qualified Data.Aeson           as Json
import qualified Data.Aeson.Encoding  as Json
import qualified Data.Aeson.Key       as Json
import qualified Data.Aeson.KeyMap    as Json
import qualified Data.Aeson.Parser    as Json
import qualified Data.Aeson.Types     as Json
import qualified Data.ByteString.Lazy as LBS
import qualified Data.Map             as Map
import qualified Data.Scientific      as Sci
import qualified Data.Text            as T
import qualified Data.Vector          as Vec

readTextFileLines = fmap (T.lines . T.pack) . readFile

loadWordEval :: String -> IO AgdaWordEval
loadWordEval fname = do
    tfl <- readTextFileLines fname
    let wkpairs = map (take 2 . T.split (==',')) tfl
    let wkmap = foldl' (\m wk -> let (w:k:_) = wk in Map.insert w (read $ T.unpack k) m) Map.empty wkpairs
    return (\ w -> Map.lookup w wkmap)

readTweets :: String -> IO [(AgdaLatLong, Text)]
readTweets fname = flip fmap (readTextFileLines fname) $ map (\fullLine -> do
    let (lat,  afterLat)  = T.break (==',') $ T.drop 1 fullLine
    let (long, afterLong) = T.break (==']') $ T.drop 2 afterLat
    -- skip [, tab, _, tab, 10 date, space, 8 time, tab ~ 6+18 = 24
    let text = T.drop 24 afterLong
    (AgdaLatLong (read $ T.unpack lat) (read $ T.unpack long) , text)
    )

readStates :: String -> IO (Either String [([[AgdaLatLong]], Text)])
readStates fname = do
    bsf <- LBS.readFile fname
    flip (maybe (return $ Left "Bad json")) (Json.decode bsf) $ \dbsf -> do 
        let (names, states) = unzip $ Json.toList dbsf
        
            pSci :: Json.Value -> Json.Parser Double
                 = Json.withScientific "Single" (return . Sci.toRealFloat)
        
            fstoll :: Vector Double -> Maybe AgdaLatLong
                = \l -> if Vec.length l >= 2 then Just $ AgdaLatLong (l Vec.! 1) (l Vec.! 0) else Nothing
        
            pLatLong :: Json.Value -> Json.Parser (Maybe AgdaLatLong)
                = Json.withArray "LatLongs" $ fmap fstoll . Vec.mapM pSci
        
            pPolygon :: Json.Value -> Json.Parser (Maybe [AgdaLatLong])
                = Json.withArray "Polygons" $ fmap (fmap Vec.toList . Vec.sequence) . Vec.mapM pLatLong

            pShape :: Json.Value -> Json.Parser (Maybe [AgdaLatLong])
                = \v -> Json.withArray "Shape" (\sh -> pPolygon $ if Vec.length sh == 1 then sh Vec.! 0 else v) v

            pState :: Json.Value -> Json.Parser (Maybe [[AgdaLatLong]])
                = Json.withArray "States" $ fmap (fmap Vec.toList . Vec.sequence) . Vec.mapM pShape

        return $ fmap (flip zip $ map Json.toText names)
               $ sequence
               $ map (join 
                        . fmap (maybe (Left "Failed parsing LatLong") Right) 
                        . Json.parseEither pState)
                    states
