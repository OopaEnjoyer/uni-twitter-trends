module TwitterTrends.Float where

open import Data.Float public
open import Data.Bool using (Bool; true; false)
open import Data.Sign as Sign using (Sign)

sin² : Float → Float
sin² x = let sx = sin x in sx * sx

cos² : Float → Float
cos² x = let cx = cos x in cx * cx

sgn : Float → Sign
sgn x with x <ᵇ 0.0
... | true  = Sign.-
... | false = Sign.+

_sgn≢ᵇ_ : Float → Float → Bool
x sgn≢ᵇ y with sgn x Sign.* sgn y
... | Sign.+ = false
... | Sign.- = true

_⊔_ : Float → Float → Float
x ⊔ y with x <ᵇ y
... | true  = y
... | false = x

_⊓_ : Float → Float → Float
x ⊓ y with x <ᵇ y
... | true  = x
... | false = y

deg⇒rad : Float → Float
deg⇒rad = _* k
    where
    k : Float
    k = 1.0 ÷ 180.0 * 3.141592654

abs : Float -> Float
abs x = x ⊔ (- x)

_≈_upto_ : Float → Float → Float → Bool
x ≈ y upto ε = abs (x - y) <ᵇ ε